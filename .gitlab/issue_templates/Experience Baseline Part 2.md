<!--
# This is a template for Part 2 of XP Baselines. 
# The issue title should be: Part 2: Experience Baseline Recommendations for [STAGE GROUP] - [JTBD]] 

-->

## Experience Baseline Recommendations Checklist

[Link to handbook page](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations/)
1. [ ] After completing the Experience Baseline for a JTBD, create a “{{YYYY}}{{Quarter}} Recommendations for…” issue for each JTBD and include them in the **Part 2: Experience Recommendations** sub-epic.
1. [ ] Brainstorm opportunities to fix or improve areas of the experience.

   Use the findings from the Emotional Grading scale to determine areas of immediate focus. For example, if parts of the experience received a “Negative” Emotional Grade, consider addressing those first. 
1. [ ] Create an issue for each recommendation and link them to the corresponding JTBD recommendations issue. 
1. [ ] Think iteratively, and create dependencies where appropriate, remembering that sometimes the order of what we release is just as important as what we release.
 
If you need to break recommendations into phases or over multiple milestones, create multiple epics and use the [Category Maturity Definitions](/direction/maturity/) in the title of each epic: **Minimal, Viable, Complete, or Lovable**.

